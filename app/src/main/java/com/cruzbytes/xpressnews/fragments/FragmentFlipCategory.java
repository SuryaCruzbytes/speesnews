package com.cruzbytes.xpressnews.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aphidmobile.flip.FlipViewController;
import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.adapter.FlipperAdapter;
import com.cruzbytes.xpressnews.api.ApiClient;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.callback.NewsFeedCallback;
import com.cruzbytes.xpressnews.model.Bookmarks;
import com.cruzbytes.xpressnews.model.LikeSend;
import com.cruzbytes.xpressnews.model.SubCategory;
import com.cruzbytes.xpressnews.model.SubcategoryList;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;

public class FragmentFlipCategory extends Fragment implements NewsFeedCallback {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String EXTRA_ID = "id";
    View view;
    String litle, shortDesc;
    private String mUrl, mNextUrl, mPreviousUrl;
    private int mId;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private MyPreference mPreference;

    private FlipperAdapter adapter;
    private AdView mAdView2;
    private InterstitialAd mInterstitialAd;
    private FlipViewController flipViewController;
    private ArrayList<SubCategory> mSubcategory;
    private NewsFeedCallback newsFeedCallback;
    private Fragment fragment;

    public FragmentFlipCategory() {
    }

    public static FragmentFlipCategory newInstance(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        FragmentFlipCategory productFragment = new FragmentFlipCategory();
        productFragment.setArguments(bundle);
        return productFragment;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_flip_news, container, false);
        processBundle();
        initObjects();
        if (!mPreference.isNewsSkip()) {
            setUrl();
        } else {
            setUrlskip();
        }
        initAds();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        flipViewController.onResume();
        if (mAdView2 != null) {
            mAdView2.resume();
            if (mInterstitialAd.isLoaded()) mInterstitialAd.show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        flipViewController.onPause();
        if (mAdView2 != null) {

            mAdView2.pause();
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        if (mAdView2 != null) {

            mAdView2.destroy();
        }
    }


    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null)
            mId = bundle.getInt(EXTRA_ID);

    }


    private void initObjects() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        flipViewController = view.findViewById(R.id.flip_view);
        mContext = getActivity();
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new MyPreference(Objects.requireNonNull(mContext));
        mSubcategory = new ArrayList<>();
        newsFeedCallback = this;
        fragment = this;
        mAdView2 = view.findViewById(R.id.adView);
        mInterstitialAd = new InterstitialAd(mContext);
        flipViewController.setOnViewFlipListener(new FlipViewController.ViewFlipListener() {
            @Override
            public void onViewFlipped(View view, int position) {


                if (position == adapter.getCount() - 1) {
                    if (!mPreference.isNewsSkip()) {
                        Log.e("position", "" + position);
                        if (mNextUrl != null) {
                            getSubcategory(mNextUrl);
                        }
                    } else {
                        if (mNextUrl != null) {
                            getSubcategorySkip(mNextUrl);
                        }
                    }
                } else if (position == 0) {
                    if (!mPreference.isNewsSkip()) {
                        if (mPreviousUrl != null) {
                            getSubcategory(mPreviousUrl);
                        }
                    } else {
                        if (mPreviousUrl != null) {
                            getSubcategorySkip(mPreviousUrl);
                        }
                    }
                }

                adapter.notifyDataSetChanged();

            }
        });
    }


    private void initAds() {

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView2.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView2.setVisibility(View.VISIBLE);
            }
        });
        mAdView2.loadAd(adRequest);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private void setUrl() {
        mUrl = ApiClient.BASE_URL + "news-blog/?limit=40";
        getSubcategory(mUrl);
    }

    private void setUrlskip() {
        mUrl = ApiClient.BASE_URL + "news-blog/?limit=40";
        getSubcategorySkip(mUrl);
    }

    private void getSubcategory(String url) {
        showProgressDialog("Loading....");
        Call<SubcategoryList> call = getApiService().getSubCategory(url, "Token " + mPreference.getToken());
        call.enqueue(new Callback<SubcategoryList>() {
            @Override
            public void onResponse(@NonNull Call<SubcategoryList> call, @NonNull Response<SubcategoryList> response) {
                SubcategoryList productResultResponse = response.body();
                if (response.isSuccessful() && productResultResponse != null) {
                    hideProgressDialog();
                    if (isAdded()) {
                        if (productResultResponse.getSubCategories().size() > 0) {

                            mSubcategory.clear();
                            Log.e("count", "" + productResultResponse.getmCount());
                            mSubcategory.addAll(productResultResponse.getSubCategories());
                            adapter = new FlipperAdapter(mContext, mSubcategory, newsFeedCallback, fragment);
                            flipViewController.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            mNextUrl = productResultResponse.getmNextUrl();
                            mPreviousUrl = productResultResponse.getmPrevious();

                        }
                    }

                } else {

                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubcategoryList> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);

            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
            mProgressDialog.setCancelable(false);
        }
    }

    private void hideProgressDialog() {


        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }

    }

    @Override
    public void onBookmarkClick(int position) {
        SubCategory subCategory = mSubcategory.get(position);

        if (!mPreference.isNewsSkip()) {
            getBookmarks(new Bookmarks(subCategory.getmId()));
        } else {
            ToastBuilder.build(mContext, "Please login to bookmark");
        }

    }

    @Override
    public void onRating(int rating, int position) {
        if (!mPreference.isNewsSkip()) {
            getlike(new LikeSend(rating), position);
        } else {
            ToastBuilder.build(mContext, "Please login to Like");
        }
    }

    @Override
    public void onShareClick(int position, View v) {

        SubCategory subCategory = mSubcategory.get(position);

        litle = subCategory.getmTitle();
        shortDesc = subCategory.getmDescription();
        if (checkAndRequestPermissions()) {
            takeScreenshot();
        } else {
            requestStoragePermission();
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_ID_MULTIPLE_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takeScreenshot();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void takeScreenshot() {
        Random r = new Random();
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + r.nextInt() + ".jpeg";

            // create bitmap screen capture
            View v1 = Objects.requireNonNull(getActivity()).getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            String filePath = imageFile.getPath();

            Log.e("dkhdf", "" + filePath);
            File file = new File(filePath);
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.putExtra(Intent.EXTRA_TEXT, litle);
            startActivity(Intent.createChooser(intent, "Share Blog"));

        } catch (Throwable e) {
            Log.e("erorr",""+e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onTitleClick(int position) {
        SubCategory subCategory = mSubcategory.get(position);

        if (subCategory.getmUrl() != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(subCategory.getmUrl()));
            startActivity(browserIntent);
        }

    }

    private void getBookmarks(Bookmarks bookmarks) {
        Call<Bookmarks> call = getApiService().bookmarks("Token " + mPreference.getToken(), bookmarks);
        call.enqueue(new Callback<Bookmarks>() {
            @Override
            public void onResponse(@NonNull Call<Bookmarks> call, @NonNull Response<Bookmarks> response) {
                Bookmarks userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    int feedId = Objects.requireNonNull(userResponse).getNewsBlog().getId();
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        SubCategory newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getmId() == feedId) {
                            newsfeeds1.setmBookmarks(true);
                            ToastBuilder.build(mContext, "News Bookmarked");
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Bookmarks> call, @NonNull Throwable t) {
                processFailure(mContext, t);
            }
        });
    }

    private void getlike(LikeSend likeSend, int position) {

        Call<LikeSend> call = getApiService().getlike("Token " + mPreference.getToken(), position, likeSend);
        call.enqueue(new Callback<LikeSend>() {

            @Override
            public void onResponse(@NonNull Call<LikeSend> call, @NonNull Response<LikeSend> response) {
                LikeSend likePojo = response.body();
                if (response.isSuccessful()) {
                    int feedId = Objects.requireNonNull(likePojo).getmId();
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        SubCategory newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getmId() == feedId) {
                            newsfeeds1.setmReacted(likePojo.getmVote());
                            adapter.notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<LikeSend> call, @NonNull Throwable t) {

                processFailure(mContext, t);

            }
        });
    }

    private void getSubcategorySkip(String url) {
        showProgressDialog("Loading....");
        Call<SubcategoryList> call = getApiService().getSubCategorySkip(url);
        call.enqueue(new Callback<SubcategoryList>() {
            @Override
            public void onResponse(@NonNull Call<SubcategoryList> call, @NonNull Response<SubcategoryList> response) {
                SubcategoryList productResultResponse = response.body();
                if (response.isSuccessful() && productResultResponse != null) {
                    if (isAdded()) {
                        hideProgressDialog();
                        if (productResultResponse.getSubCategories().size() > 0) {
                            mSubcategory.clear();
                            mSubcategory.addAll(productResultResponse.getSubCategories());
                            adapter = new FlipperAdapter(mContext, mSubcategory, newsFeedCallback, fragment);
                            flipViewController.setAdapter(adapter);
                            mNextUrl = productResultResponse.getmNextUrl();
                            mPreviousUrl = productResultResponse.getmPrevious();
                            adapter.notifyDataSetChanged();
                        }
                    }

                } else {
                    hideProgressDialog();
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubcategoryList> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);
            }
        });
    }
}
