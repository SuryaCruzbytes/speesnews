package com.cruzbytes.xpressnews.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aphidmobile.flip.FlipViewController;
import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.adapter.FlipperBookmarksAdapter;
import com.cruzbytes.xpressnews.api.ApiClient;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.callback.NewsFeedCallback;
import com.cruzbytes.xpressnews.model.Bookmarks;
import com.cruzbytes.xpressnews.model.LikeSend;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;

public class FragmentFlipBookmarks extends Fragment implements NewsFeedCallback {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String EXTRA_ID = "id";
    View view;
    int i = 0, j = 0, k = 0;
    String litle, shortDesc;
    NewsFeedCallback newsFeedCallback;
    Fragment fragment;
    private String mUrl;
    private int mId;
    private ArrayList<Bookmarks> mSubcategory;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private MyPreference mPreference;
    private LinearLayout mLayoutEmpty;
    private FlipperBookmarksAdapter adapter;
    private FlipViewController flipViewController;

    public FragmentFlipBookmarks() {
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_flip_bookmarks, container, false);
        initObjects();

        return view;
    }

    private void initObjects() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        mLayoutEmpty = view.findViewById(R.id.empty);
        flipViewController = view.findViewById(R.id.flip_view);
        mContext = getActivity();
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new MyPreference(Objects.requireNonNull(mContext));
        mSubcategory = new ArrayList<>();
        newsFeedCallback = this;
        fragment = this;
        flipViewController.setOnViewFlipListener(new FlipViewController.ViewFlipListener() {
            @Override
            public void onViewFlipped(View view, int position) {
                adapter.notifyDataSetChanged();
            }
        });
        if (!mPreference.isNewsSkip()) {
            getSubcategory();
        } else {
            ToastBuilder.build(mContext, "Please Login to view Bookmarks");
        }
    }

    private void getSubcategory() {
        showProgressDialog("Loading...");
        Call<ArrayList<Bookmarks>> call = getApiService().getMyBookmarks("Token " + mPreference.getToken());
        call.enqueue(new Callback<ArrayList<Bookmarks>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Bookmarks>> call, @NonNull Response<ArrayList<Bookmarks>> response) {
                ArrayList<Bookmarks> bookmarkslist = response.body();
                if (response.isSuccessful() && bookmarkslist != null) {

                    if (isAdded()) {
                        hideProgressDialog();
                        if (bookmarkslist.size() > 0) {
                            mSubcategory.clear();
                            mSubcategory.addAll(bookmarkslist);
                            adapter = new FlipperBookmarksAdapter(mContext, mSubcategory, newsFeedCallback, fragment);
                            flipViewController.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                            mLayoutEmpty.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<Bookmarks>> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);

            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onBookmarkClick(int position) {

        Bookmarks bookmarks = mSubcategory.get(position);
        mId = bookmarks.getNewsBlog().getId();
        String Url = ApiClient.BASE_URL + "news-blog/bookmark/" + bookmarks.getmId() + "/";
        deletBookmarks(Url, mId, position);


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onRating(int rating, int position) {
        getlike(new LikeSend(rating), position);
    }


    @Override
    public void onShareClick(int position, View v) {
        Bookmarks subCategory = mSubcategory.get(position);

        litle = subCategory.getNewsBlog().getmTitle();
        shortDesc = subCategory.getNewsBlog().getmDescription();
        if (checkAndRequestPermissions()) {
            takeScreenshot();
        } else {
            requestStoragePermission();
        }
    }

    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.READ_EXTERNAL_STORAGE);
        int locationPermission = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_ID_MULTIPLE_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takeScreenshot();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void takeScreenshot() {
        Random r = new Random();
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + r.nextInt() + ".jpeg";

            // create bitmap screen capture
            View v1 = Objects.requireNonNull(getActivity()).getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            String filePath = imageFile.getPath();

            Log.e("dkhdf", "" + filePath);
            File file = new File(filePath);
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.putExtra(Intent.EXTRA_TEXT, litle);
            startActivity(Intent.createChooser(intent, "Share Blog"));

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTitleClick(int position) {
        Bookmarks subCategory = mSubcategory.get(position);
        if (subCategory.getNewsBlog().getmUrl() != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(subCategory.getNewsBlog().getmUrl()));
            startActivity(browserIntent);
        }

    }

    private void deletBookmarks(String Url, final int id, final int position) {
        Call<Bookmarks> call = getApiService().deleteBookmarks(Url, "Token " + mPreference.getToken());
        call.enqueue(new Callback<Bookmarks>() {
            @Override
            public void onResponse(@NonNull Call<Bookmarks> call, @NonNull Response<Bookmarks> response) {
                if (response.code() == 204) {
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        Bookmarks newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getNewsBlog().getId() == id) {
                            newsfeeds1.getNewsBlog().setBookmarks(false);
                            ToastBuilder.build(mContext, "Bookmark removed");
                            adapter.notifyDataSetChanged();
                            if (position > 0) {
                                removeItem(position);
                            }

                        }
                    }
                } else {
                    //  processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Bookmarks> call, @NonNull Throwable t) {
                processFailure(mContext, t);
            }
        });
    }

    private void removeItem(int position) {
        mSubcategory.remove(position);
        adapter.notifyDataSetChanged();
    }


    private void getlike(LikeSend likeSend, int position) {

        Call<LikeSend> call = getApiService().getlike("Token " + mPreference.getToken(), position, likeSend);
        call.enqueue(new Callback<LikeSend>() {

            @Override
            public void onResponse(@NonNull Call<LikeSend> call, @NonNull Response<LikeSend> response) {
                LikeSend likePojo = response.body();
                Log.e("bookmark", "" + response.toString());
                if (response.isSuccessful()) {
                    int feedId = Objects.requireNonNull(likePojo).getmId();
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        Bookmarks newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getmId() == feedId) {
                            newsfeeds1.getNewsBlog().setmReacted(likePojo.getmVote());
                            adapter.notifyDataSetChanged();
                        }


                    }
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));

                }
            }

            @Override
            public void onFailure(@NonNull Call<LikeSend> call, @NonNull Throwable t) {

                processFailure(mContext, t);

            }
        });
    }

}
