package com.cruzbytes.xpressnews.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.callback.CategoryCallback;
import com.cruzbytes.xpressnews.model.CategoryHolder;
import com.cruzbytes.xpressnews.model.CategoryPojo;

import java.util.ArrayList;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {


    public Context mContext;
    private CategoryCallback mCallback;

    private ArrayList<CategoryPojo> mBrowseplanList;

    public CategoryAdapter(Context context, ArrayList<CategoryPojo> myPhotoList, CategoryCallback callback) {
        mContext = context;
        mBrowseplanList = myPhotoList;
        mCallback = callback;

    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, final int position) {
        final CategoryPojo operatorsData = mBrowseplanList.get(position);
        holder.mOperatorType.setText(operatorsData.getName());

        if (operatorsData.getId() == 1) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_political);
        } else if (operatorsData.getId() == 2) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_tamilnadu_news);
        } else if (operatorsData.getId() == 3) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_india);
        } else if (operatorsData.getId() == 4) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_world);
        } else if (operatorsData.getId() == 5) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_sports);
        } else if (operatorsData.getId() == 6) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_cinema_new);
        } else if (operatorsData.getId() == 7) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_economy);
        } else if (operatorsData.getId() == 8) {
            holder.mOperatorLogo.setImageResource(R.drawable.ic_meme);
        }
    }

    @Override
    public int getItemCount() {
        return mBrowseplanList.size();
    }


}
