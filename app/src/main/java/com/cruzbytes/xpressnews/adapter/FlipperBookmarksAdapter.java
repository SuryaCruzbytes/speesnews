package com.cruzbytes.xpressnews.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cruzbytes.inlineyoutube.YouTubePlayerView;
import com.cruzbytes.inlineyoutube.models.ImageLoader;
import com.cruzbytes.inlineyoutube.models.YouTubePlayerType;
import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.callback.NewsFeedCallback;
import com.cruzbytes.xpressnews.model.Bookmarks;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Objects;

import static com.cruzbytes.xpressnews.Utils.Utils.getRelativeTime;

public class FlipperBookmarksAdapter extends BaseAdapter {
    private static final String API_KEY = "AIzaSyBWElbrRFFd1PhKh-sC55FNgwGD9lmE_Cg";
    public Dialog dialog;
    String VideoId;
    private Context mContext;
    private ArrayList<Bookmarks> Data;
    private NewsFeedCallback mCallback;
    private Fragment mfragment;
    private ImageLoader imageLoader = new ImageLoader() {
        @Override
        public void loadImage(@NonNull ImageView imageView, @NonNull String url, int height, int width) {
            Picasso.with(imageView.getContext()).load(url).resize(width, height).centerCrop().into(imageView);
        }
    };

    public FlipperBookmarksAdapter(Context context, ArrayList<Bookmarks> objects, NewsFeedCallback newsFeedCallback, Fragment fragment) {
        super();
        this.Data = objects;
        this.mContext = context;
        mCallback = newsFeedCallback;
        mfragment = fragment;
    }

    @Override
    public int getCount() {
        return Data.size();
    }

    @Override
    public Object getItem(int position) {
        return Data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.item_news_bookmarks, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.mTextViewNewsTitle.setText(Data.get(position).getNewsBlog().getmTitle());
            holder.mTextViewNewsDescription.setText(Data.get(position).getNewsBlog().getmDescription());
            holder.mTextViewTime.setText(getRelativeTime(Data.get(position).getNewsBlog().getmCreatedOn()));
            YouTubePlayerView playerView = holder.videoView;
            if (Data.get(position).getNewsBlog().getmVideoUrl() != null) {
                if (Data.get(position).getNewsBlog().getmVideoUrl().contains("&feature=youtu.be")) {
                    String parts = Data.get(position).getNewsBlog().getmVideoUrl().replace("&feature=youtu.be", "");
                    String spilt2[] = parts.split("\\=");
                    VideoId = spilt2[1];
                } else if (Data.get(position).getNewsBlog().getmVideoUrl().contains("watch?v=")) {
                    String parts[] = Data.get(position).getNewsBlog().getmVideoUrl().split("\\=");
                    VideoId = parts[1];
                }

                holder.mImageViewNews.setVisibility(View.GONE);
                Glide.with(mContext).load("").into(holder.mImageViewNews);
                holder.videoView.setVisibility(View.VISIBLE);
                playerView.initPlayer(API_KEY, VideoId, "https://cdn.rawgit.com/flipkart-incubator/inline-youtube-view/60bae1a1/youtube-android/youtube_iframe_player.html",
                        YouTubePlayerType.WEB_VIEW, null, mfragment, imageLoader);

            } else {
                playerView.unbindPlayer();
                String data = Data.get(position).getNewsBlog().getmImage();
                holder.mImageViewNews.setVisibility(View.VISIBLE);
                Glide.with(mContext).load(data).into(holder.mImageViewNews);
                holder.videoView.setVisibility(View.GONE);
            }
            holder.mImageBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onBookmarkClick(position);
                }
            });
            holder.mImageShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onShareClick(position, v);
                }
            });
            holder.mTextViewNewsTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTitleClick(position);
                }
            });
            if (Data.get(position).getNewsBlog().isBookmarks()) {
                holder.mImageBookmark.setImageResource(R.drawable.ic_bookmark_selected);
            } else {
                holder.mImageBookmark.setImageResource(R.drawable.ic_bookmark);
            }
            holder.mSmileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
                @Override
                public void onRatingSelected(int level, boolean reselected) {
                    @BaseRating.Smiley int smiley = holder.mSmileRating.getSelectedSmile();
                    int mRating = holder.mSmileRating.getRating();
                    mCallback.onRating(mRating, Data.get(position).getNewsBlog().getId());
                }
            });
            if (Data.get(position).getNewsBlog().getmReacted() == 1) {
                holder.mSmileRating.setSelectedSmile(0);
            } else if (Data.get(position).getNewsBlog().getmReacted() == 2) {
                holder.mSmileRating.setSelectedSmile(1);
            } else if (Data.get(position).getNewsBlog().getmReacted() == 3) {
                holder.mSmileRating.setSelectedSmile(2);
            } else if (Data.get(position).getNewsBlog().getmReacted() == 4) {
                holder.mSmileRating.setSelectedSmile(3);
            } else if (Data.get(position).getNewsBlog().getmReacted() == 5) {
                holder.mSmileRating.setSelectedSmile(4);
            } else {
                holder.mSmileRating.setSelectedSmile(-1);
            }
            holder.mImageViewNews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogImage(Data.get(position).getNewsBlog().getmImage());
                }
            });
        }

        return convertView;
    }

    private void dialogImage(String ImageUrl) {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_imageview);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(Objects.requireNonNull(dialog.getWindow()).getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
        ImageView imageView = dialog.findViewById(R.id.imageviewDetail);
        ImageView mImageBack = dialog.findViewById(R.id.img_back);
        Glide.with(mContext).load(ImageUrl).into(imageView);
        mImageBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private static class ViewHolder {
        public TextView mTextViewNewsTitle, mTextViewNewsDescription, mTextViewTime;
        public TextView title;
        public ImageView mImageViewNews, mImageBookmark, mImageShare;
        public RelativeLayout mLayoutmain;
        public YouTubePlayerView videoView;
        private SmileRating mSmileRating;

        public ViewHolder(View itemView) {
            mImageViewNews = itemView.findViewById(R.id.image_news);
            mTextViewNewsTitle = itemView.findViewById(R.id.txt_newstitle);
            mTextViewNewsDescription = itemView.findViewById(R.id.txt_newsdescription);
            mTextViewTime = itemView.findViewById(R.id.txt_time);
            videoView = itemView.findViewById(R.id.youtube_player_view);
            mImageBookmark = itemView.findViewById(R.id.img_bookmarks);
            mLayoutmain = itemView.findViewById(R.id.main_newsLayout);
            mSmileRating = itemView.findViewById(R.id.smile_rating);
            mImageShare = itemView.findViewById(R.id.img_share);

        }

    }


}
