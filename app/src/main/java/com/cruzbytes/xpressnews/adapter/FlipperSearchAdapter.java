package com.cruzbytes.xpressnews.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.callback.NewsFeedCallback;
import com.cruzbytes.xpressnews.model.SubCategory;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;

import java.util.ArrayList;
import java.util.Objects;

import static com.cruzbytes.xpressnews.Utils.Utils.getRelativeTime;

public class FlipperSearchAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SubCategory> Data;
    private NewsFeedCallback mCallback;

    public FlipperSearchAdapter(Context context, ArrayList<SubCategory> objects, NewsFeedCallback newsFeedCallback) {
        super();
        this.Data = objects;
        this.mContext = context;
        mCallback = newsFeedCallback;
    }

    @Override
    public int getCount() {
        return Data.size();
    }

    @Override
    public Object getItem(int position) {
        return Data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = Objects.requireNonNull(inflater).inflate(R.layout.item_news_search, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();

            holder.mTextViewNewsTitle.setText(Data.get(position).getmTitle());
            holder.mTextViewNewsDescription.setText(Data.get(position).getmDescription());
            holder.mTextViewTime.setText(getRelativeTime(Data.get(position).getmCreatedOn()));
            Glide.with(mContext).load(Data.get(position).getmImage()).into(holder.mImageViewNews);

            holder.mImageBookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onBookmarkClick(position);
                }
            });
            holder.mImageShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onShareClick(position, v);
                }
            });
            holder.mTextViewNewsTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onTitleClick(position);
                }
            });
            if (Data.get(position).getmBookmarks()) {
                holder.mImageBookmark.setImageResource(R.drawable.ic_bookmark_selected);
            } else {
                holder.mImageBookmark.setImageResource(R.drawable.ic_bookmark);
            }


            holder.mSmileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
                @Override
                public void onRatingSelected(int level, boolean reselected) {
                    @BaseRating.Smiley int smiley = holder.mSmileRating.getSelectedSmile();
                    int mRating = holder.mSmileRating.getRating();
                    mCallback.onRating(mRating, Data.get(position).getmId());
                }
            });
            Log.e("getmReacted", "" + Data.get(position).getmReacted());
            if (Data.get(position).getmReacted() == 1) {
                holder.mSmileRating.setSelectedSmile(0);
            } else if (Data.get(position).getmReacted() == 2) {
                holder.mSmileRating.setSelectedSmile(1);
            } else if (Data.get(position).getmReacted() == 3) {
                holder.mSmileRating.setSelectedSmile(2);
            } else if (Data.get(position).getmReacted() == 4) {
                holder.mSmileRating.setSelectedSmile(3);
            } else if (Data.get(position).getmReacted() == 5) {
                holder.mSmileRating.setSelectedSmile(4);
            } else {
                holder.mSmileRating.setSelectedSmile(-1);
            }

        }

        return convertView;
    }

    private static class ViewHolder {
        public TextView mTextViewNewsTitle, mTextViewNewsDescription, mTextViewTime, mTextviewLikeCount, mTextViewDislikeCount;
        public TextView title;
        public ImageView mImageViewNews, mImageBookmark, mImageShare, mImageLike, mImageDislike;
        public RelativeLayout mLayoutmain;
        private SmileRating mSmileRating;
        public ViewHolder(View itemView) {
            mImageViewNews = itemView.findViewById(R.id.image_news);
            mTextViewNewsTitle = itemView.findViewById(R.id.txt_newstitle);
            mTextViewNewsDescription = itemView.findViewById(R.id.txt_newsdescription);
            mTextViewTime = itemView.findViewById(R.id.txt_time);
            mImageBookmark = itemView.findViewById(R.id.img_bookmarks);

            mLayoutmain = itemView.findViewById(R.id.main_newsLayout);
            mSmileRating = itemView.findViewById(R.id.smile_rating);
            mImageShare = itemView.findViewById(R.id.img_share);

        }

    }
}
