package com.cruzbytes.xpressnews;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.cruzbytes.xpressnews.adapter.CategoryAdapter;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.callback.CategoryCallback;
import com.cruzbytes.xpressnews.model.CategoryPojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.nfc.NfcAdapter.EXTRA_ID;
import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;
import static com.cruzbytes.xpressnews.app.MyActivity.launchClearStack;
import static com.cruzbytes.xpressnews.app.MyActivity.launchWithBundle;

public class CategoriesActivity extends AppCompatActivity implements View.OnClickListener, CategoryCallback {

    MyPreference myPreference;
    LinearLayoutManager mLayoutManager;
    CategoryAdapter mAdapter;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private RecyclerView mRecyclerView;
    private ArrayList<CategoryPojo> mCategoryList;
    private ImageView mImageBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        initObjects();
        initCallbacks();
        initRecyclerView();
        getCategory();
    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            onBackPressed();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        mRecyclerView = findViewById(R.id.recyclerview);
        mImageBack = findViewById(R.id.img_arrow);
        mProgressDialog = new ProgressDialog(mContext);
        myPreference = new MyPreference(mContext);
        mCategoryList = new ArrayList<>();
        mAdapter = new CategoryAdapter(mContext, mCategoryList, this);
        mLayoutManager = new GridLayoutManager(mContext, 3);

    }

    private void initRecyclerView() {
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onItemClick(int position) {
        CategoryPojo subCategory = mCategoryList.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, subCategory.getId());
        bundle.putString("title", subCategory.getName());

        launchWithBundle(mContext, CategoryDetailActivity.class, bundle);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getCategory() {

        Call<ArrayList<CategoryPojo>> call = getApiService().getCategory();

        call.enqueue(new Callback<ArrayList<CategoryPojo>>() {

            @Override
            public void onResponse(@NonNull Call<ArrayList<CategoryPojo>> call, @NonNull Response<ArrayList<CategoryPojo>> response) {

                List<CategoryPojo> subCategoryListResponse = response.body();
                if (response.isSuccessful() && subCategoryListResponse != null) {
                    mCategoryList.clear();
                    mCategoryList.addAll(subCategoryListResponse);
                    mAdapter.notifyDataSetChanged();
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ArrayList<CategoryPojo>> call, @NonNull Throwable t) {
                processFailure(mContext, t);

            }
        });
    }

    @Override
    public void onBackPressed() {
        launchClearStack(mContext, MainActivity.class);
        super.onBackPressed();

    }
}
