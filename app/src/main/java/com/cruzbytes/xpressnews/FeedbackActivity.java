package com.cruzbytes.xpressnews;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.model.Rating;
import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView mImageClose;
    private EditText mEditReason;
    private Context mContext;
    private Button mButtonFeedback;
    private SmileRating mSmileRating;
    private String mfeedback;
    private int mRating;
    private ProgressDialog mProgressDialog;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initObjects();
        initCallbacks();

    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
        mButtonFeedback.setOnClickListener(this);
    }

    private void initObjects() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mContext = this;
        mPreference = new MyPreference(mContext);

        mProgressDialog = new ProgressDialog(mContext);
        mImageClose = findViewById(R.id.img_arrow);
        mEditReason = findViewById(R.id.edit_reason);
        mButtonFeedback = findViewById(R.id.btn_submit);
        mSmileRating = findViewById(R.id.smile_rating);
        mSmileRating.setOnRatingSelectedListener(new SmileRating.OnRatingSelectedListener() {
            @Override
            public void onRatingSelected(int level, boolean reselected) {
                @BaseRating.Smiley int smiley = mSmileRating.getSelectedSmile();
                switch (smiley) {
                    case SmileRating.BAD:
                        mfeedback = "Bad";
                        break;
                    case SmileRating.GOOD:
                        mfeedback = "Good";
                        break;
                    case SmileRating.GREAT:
                        mfeedback = "Great";
                        break;
                    case SmileRating.OKAY:
                        mfeedback = "Okay";
                        break;
                    case SmileRating.TERRIBLE:
                        mfeedback = "Terrible";
                        break;
                }
                mRating = mSmileRating.getRating();
                Log.e("mRating", "" + mRating);
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        } else if (v == mButtonFeedback) {
            if (!mPreference.isNewsSkip()) {
                if (mRating > 0) {
                    rating(new Rating(mRating, mEditReason.getText().toString()));
                } else {
                    ToastBuilder.build(mContext, "Please give your experience");
                }
            } else {
                ToastBuilder.build(mContext, "Unable to send Feedback,Please Login");
            }
        }
    }

    private void rating(Rating rating) {

        showProgressDialog("Processing....");
        Call<Rating> call = getApiService().RatingFeed("Token " + mPreference.getToken(), rating);
        call.enqueue(new Callback<Rating>() {
            @Override
            public void onResponse(@NonNull Call<Rating> call, @NonNull Response<Rating> response) {
                Rating rating = response.body();
                if (response.isSuccessful() && rating != null) {
                    hideProgressDialog();
                    ToastBuilder.build(mContext, "Thank you for your valuable feedback !!");
                    onBackPressed();
                } else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));


                }
            }

            @Override
            public void onFailure(@NonNull Call<Rating> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);
            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
