package com.cruzbytes.xpressnews.callback;

import android.support.v4.app.Fragment;


public interface FragmentCallback {
    void launchFragment(Fragment fragment, boolean addToBackStack);

}
