package com.cruzbytes.xpressnews.callback;

import android.view.View;

/**
 * Created by Bobby on 15/07/17
 */

public interface NewsFeedCallback {
    void onBookmarkClick(int position);

    void onRating(int rating, int position);
    void onShareClick(int position, View v);
    void onTitleClick(int position);
}
