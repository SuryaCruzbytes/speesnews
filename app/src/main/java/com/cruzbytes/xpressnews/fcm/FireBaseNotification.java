package com.cruzbytes.xpressnews.fcm;


import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.cruzbytes.xpressnews.MainActivity;
import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.model.NotificationMessage;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static android.nfc.NfcAdapter.EXTRA_ID;


public class FireBaseNotification extends FirebaseMessagingService {
    private static final String NOTIFY_ID = "Xpress_News";
    private static final String NOTIFY_CHANNEL = "Xpress_News";
    private static final int REQUEST_NOTIFICATION = 4;
    Bitmap bitmap;

    @Override
    public void onNewToken(String token) {

        storeToken(token);
        super.onNewToken(token);
    }

    private void storeToken(String token) {
        MyPreference myPreference = new MyPreference(this);
        myPreference.setFcmToken(token);
        Log.e("token", "" + token);
        myPreference.setTokenUploaded(false);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Gson gson = new Gson();
        String message = gson.toJson(remoteMessage.getData());
        NotificationMessage notificationMessage = gson.fromJson(message, NotificationMessage.class);
        Log.e("received", "" + message);
        MyPreference preference = new MyPreference(this);
        String token = preference.getToken();
        if (token != null) {
            displayBlogNotification(notificationMessage);
        }
    }
    @SuppressLint("WrongConstant")
    private void displayBlogNotification(NotificationMessage notificationMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_ID, notificationMessage.getmId());
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(notificationMessage.getmImage()).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this,
                notificationMessage.getmId(), intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFY_ID);
        builder.setColor(ContextCompat.getColor(this, R.color.primary))
                .setTicker(notificationMessage.getmTitle())
                .setContentTitle(notificationMessage.getmTitle())
                .setContentText(notificationMessage.getmDesc())
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        if (bitmap != null)
            builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
        NotificationManager notificationManager = (NotificationManager) getSystemService(
                Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(notificationMessage.getmId(), builder.build());
        }
    }
}
