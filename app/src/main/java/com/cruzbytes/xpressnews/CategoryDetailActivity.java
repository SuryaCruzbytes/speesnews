package com.cruzbytes.xpressnews;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cruzbytes.xpressnews.callback.FragmentCallback;
import com.cruzbytes.xpressnews.fragments.FragmentFlipCategoryDetail;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.nfc.NfcAdapter.EXTRA_ID;
import static com.cruzbytes.xpressnews.app.MyActivity.launchClearStack;

public class CategoryDetailActivity extends AppCompatActivity implements FragmentCallback, View.OnClickListener {

    private int mId;
    protected TextView mTitle;
    String mNewsTitle;
    private ImageView mImageBack;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        processBundle();
        initCallbacks();
        context = this;

    }

    private void initCallbacks() {
        mImageBack.setOnClickListener(this);
    }

    private void processBundle() {

        mTitle = findViewById(R.id.txt_title);
        mImageBack = findViewById(R.id.img_arrow);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mId = bundle.getInt(EXTRA_ID);
            mNewsTitle = bundle.getString("title");

            mTitle.setText(mNewsTitle);
            launchFragment(FragmentFlipCategoryDetail.newInstance(mId), false);
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void launchFragment(android.support.v4.app.Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        if (v == mImageBack) {
            launchClearStack(context, CategoriesActivity.class);
        }
    }
}
