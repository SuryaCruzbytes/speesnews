package com.cruzbytes.xpressnews;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.cruzbytes.xpressnews.app.MyPreference;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.app.MyActivity.launchClearStack;

public class SplashActivity extends AppCompatActivity {

    Context mContext;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initObjects();
        final Thread mythread = new Thread() {
            @SuppressWarnings("StatementWithEmptyBody")
            public void run() {
                try {
                    sleep(1000);
                } catch (Exception ignored) {
                } finally {
                    if (mPreference.getToken() != null) {
                        launchClearStack(mContext, MainActivity.class);
                    } else {
                        launchClearStack(mContext, LoginActivity.class);
                    }
                }
            }


        };
        mythread.start();

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mPreference = new MyPreference(mContext);
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }
}
