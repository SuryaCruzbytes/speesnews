package com.cruzbytes.xpressnews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.helper.MyBounceInterpolator;
import com.cruzbytes.xpressnews.helper.RevealAnimation;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.app.MyActivity.launchWithBundle;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener {


    RevealAnimation mRevealAnimation;
    private Context mContext;
    private ImageView mImageclose, mImageSearch;
    private LinearLayout mLayoutSearch, mLayoutActivity;
    private EditText mEditSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initObjects();
        initCallbacks();

    }

    private void initCallbacks() {
        mImageclose.setOnClickListener(this);
        mImageSearch.setOnClickListener(this);
    }

    private void initObjects() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mLayoutActivity = findViewById(R.id.mLayout_searchActivity);

        Intent intent = this.getIntent();
        mRevealAnimation = new RevealAnimation(mLayoutActivity, intent, this);
        mContext = this;
        mImageclose = findViewById(R.id.img_close);
        mLayoutSearch = findViewById(R.id.layout_search);
        mImageSearch = findViewById(R.id.img_search);
        mEditSearch = findViewById(R.id.edit_searchkey);
        final Animation myAnim = AnimationUtils.loadAnimation(mContext, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 20);
        myAnim.setInterpolator(interpolator);
        myAnim.setDuration(1000);
        mLayoutSearch.startAnimation(myAnim);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageclose) {
            onBackPressed();
        } else if (v == mImageSearch) {

            if (!mEditSearch.getText().toString().isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putString("SearchKey", mEditSearch.getText().toString());
                launchWithBundle(mContext, SearchDetailActivity.class, bundle);

            }
            else
            {
                ToastBuilder.build(mContext,"Search cannot be empty");
            }

        }
    }

    @Override
    public void onBackPressed() {
        mRevealAnimation.unRevealActivity();
    }
}
