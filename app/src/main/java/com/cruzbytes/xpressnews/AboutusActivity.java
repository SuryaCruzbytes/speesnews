package com.cruzbytes.xpressnews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutusActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView mTextViewContent;
    private ImageView mImageClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        initObjects();
        initCallbacks();
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("SetTextI18n")
    private void initObjects() {
        mTextViewContent = findViewById(R.id.aboutus_content);
        mImageClose = findViewById(R.id.img_arrow);
        mTextViewContent.setText("Xpress News - தமிழில் எளிமையாகவும், சுருக்கமாகவும், விரைவாகவும் செய்தி வழங்குவதற்காக தொடங்கப்பட்டுள்ளது. அவசர உலகில் ஒரு நிமிடத்திற்குள் ஒவ்வொரு செய்தியையும் படித்துவிடும் வகையில் வடிவமைக்கப்பட்டுள்ளது Xpress News app. மொபைல் வடிவில் உலகத்தை கைக்குள் வைத்திருக்கும் உங்களுக்கு,விரல் நுனியில் உலகத்தின் செய்திகளை வழங்குகிறது Xpress News. உங்கள் ஆதரவை தொடர்ந்து வழங்குங்கள். நன்றி!");
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        }
    }
}
