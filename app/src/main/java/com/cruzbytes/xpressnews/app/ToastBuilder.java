package com.cruzbytes.xpressnews.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cruzbytes.xpressnews.R;

/**
 * Created by Bobby on 13/07/17
 */

public class ToastBuilder {
    public static void build(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void success(Context context, String message) {
        Toast toast = new Toast(context);
        toast.setView(getToastView(context, R.drawable.bg_success, R.drawable.ic_success, message));
        toast.show();
    }

    public static void failure(Context context, String message) {
        Toast toast = new Toast(context);
        toast.setView(getToastView(context, R.drawable.bg_failure, R.drawable.ic_failure, message));
        toast.show();
    }

    private static View getToastView(Context context, int background, int icon, String message) {
        View toastView = LayoutInflater.from(context).inflate(R.layout.toast, null);
        RelativeLayout layout = toastView.findViewById(R.id.toast);
        layout.setBackgroundResource(background);
        ImageView imageView = toastView.findViewById(R.id.img_toast);
        imageView.setImageResource(icon);
        TextView textView = toastView.findViewById(R.id.txt_toast);
        textView.setText(message);
        return toastView;
    }

}
