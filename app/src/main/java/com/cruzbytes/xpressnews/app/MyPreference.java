package com.cruzbytes.xpressnews.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by Bobby on 29/07/17
 */

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String PREF_LOCATION = "location";
    private static final String PREF_EXTRAS = "extras";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String TOKEN = "token";
    private static final String FCM_TOKEN = "device_token";

    private static final String PROFILE_PIC = "profile_pic";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private static final String NEWS_SKIP = "news_skip";

    private SharedPreferences mPreferencesUser, mPreferencesExtras;

    public MyPreference(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        mPreferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
    }

    public int getId() {
        return mPreferencesUser.getInt(encode(ID), -1);
    }

    public void setId(int id) {
        mPreferencesUser.edit().putInt(encode(ID), id).apply();
    }

    public String getName() {
        return mPreferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public String getPhone() {
        return mPreferencesUser.getString(encode(PHONE), null);
    }

    public void setPhone(String phone) {
        mPreferencesUser.edit().putString(encode(PHONE), phone).apply();
    }

    public String getEmail() {
        return mPreferencesUser.getString(encode(EMAIL), null);
    }

    public void setEmail(String email) {
        mPreferencesUser.edit().putString(encode(EMAIL), email).apply();
    }

    public String getToken() {
        return mPreferencesUser.getString(encode(TOKEN), null);
    }

    public void setToken(String token) {
        mPreferencesUser.edit().putString(encode(TOKEN), token).apply();
    }

    public String getProfilePic() {
        return mPreferencesUser.getString(encode(PROFILE_PIC), null);
    }

    public void setProfilePic(String profilePic) {
        mPreferencesUser.edit().putString(encode(PROFILE_PIC), profilePic).apply();
    }


    public boolean isTokenUploaded() {
        return mPreferencesExtras.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        mPreferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public boolean isNewsSkip() {
        return mPreferencesExtras.getBoolean(encode(NEWS_SKIP), false);
    }

    public void setNewsSkip(boolean newsSkip) {
        mPreferencesExtras.edit().putBoolean(encode(NEWS_SKIP), newsSkip).apply();
    }

    public void clearUser() {
        setTokenUploaded(false);
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }


    public String getFcmToken() {
        return mPreferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    public void setFcmToken(String fcmToken) {
        mPreferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

}
