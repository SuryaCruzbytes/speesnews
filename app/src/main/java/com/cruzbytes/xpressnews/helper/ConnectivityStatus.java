package com.cruzbytes.xpressnews.helper;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Bobby on 27/09/17
 */

public class ConnectivityStatus extends ContextWrapper {

    public ConnectivityStatus(Context base) {
        super(base);
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = manager != null ? manager.getActiveNetworkInfo() : null;
        return connection != null && connection.isConnectedOrConnecting();
    }
}
