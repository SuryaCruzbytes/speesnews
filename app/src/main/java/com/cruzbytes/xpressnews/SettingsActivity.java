package com.cruzbytes.xpressnews;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.model.Profile;
import com.suke.widget.SwitchButton;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {
    protected EditText mEditUsername, mEditEmail, mEditMobile;
    protected SwitchButton switchButton;
    private Context mContext;
    private ImageView mImageClose;
    private ProgressDialog mProgressDialog;
    private MyPreference mPreference;
    private Button mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initObjects();
        initCallbacks();
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
        mSave.setOnClickListener(this);
    }

    private void initObjects() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new MyPreference(mContext);
        mImageClose = findViewById(R.id.img_arrow);
        mEditUsername = findViewById(R.id.usr_name);
        mEditEmail = findViewById(R.id.usr_mail);
        mEditMobile = findViewById(R.id.usr_mobile);
        switchButton = findViewById(R.id.switch_button);
        mSave = findViewById(R.id.btn_nextstep);
        mProgressDialog = new ProgressDialog(mContext);
        switchButton.isChecked();
        switchButton.toggle();
        switchButton.setShadowEffect(true);
        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
            }
        });
        mEditUsername.setText(mPreference.getName());
        if (mPreference.getPhone() != null) {
            mEditMobile.setText(mPreference.getPhone());
        } else {
            mEditMobile.setText("0987655354");
        }

        mEditEmail.setText(mPreference.getEmail());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        } else if (v == mSave) {

            if (!mPreference.isNewsSkip()) {
                showProgressDialog("Loading...");
                getProfile(new Profile(mEditMobile.getText().toString(), mEditUsername.getText().toString(),
                        "", mEditEmail.getText().toString()));
            } else {
                ToastBuilder.build(mContext, "Unable to Change the Profile, Please Login ");
            }
        }
    }

    private void getProfile(Profile profile) {
        Call<Profile> call = getApiService().UpdateProfile("Token " + mPreference.getToken(), profile);
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(@NonNull Call<Profile> call, @NonNull Response<Profile> response) {

                Profile userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    hideProgressDialog();

                    mPreference.setEmail(userResponse.getmEmail());
                    mPreference.setName(userResponse.getmFirstName());
                    mPreference.setPhone(userResponse.getmUsername());

                    ToastBuilder.build(mContext, "Updated Successfully");
                    onBackPressed();
                } else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Profile> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);
            }
        });
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
