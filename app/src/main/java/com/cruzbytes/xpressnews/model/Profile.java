package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;


public class Profile {


    @SerializedName("id")
    private String id;
    @SerializedName("username")
    private String mUsername;

    @SerializedName("first_name")
    private String mFirstName;
    @SerializedName("last_name")
    private String mLastName;
    @SerializedName("email")
    private String mEmail;



    public Profile(String username,String Firstname,String LastName,String Email)
    {
        mUsername=username;
        mFirstName=Firstname;
        mLastName=LastName;
        mEmail=Email;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }
}
