package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by admin on 10/5/2017.
 */

public class SubcategoryList {
    @SerializedName("id")
    private int mId;
    @SerializedName("count")
    private String mCount;
    @SerializedName("previous")
    private String mPrevious;
    @SerializedName("next")
    private String mNextUrl;
    @SerializedName("results")
    public ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();


    public String getmNextUrl() {
        return mNextUrl;
    }

    public void setmNextUrl(String mNextUrl) {
        this.mNextUrl = mNextUrl;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmCount() {
        return mCount;
    }

    public void setmCount(String mCount) {
        this.mCount = mCount;
    }

    public String getmPrevious() {
        return mPrevious;
    }

    public void setmPrevious(String mPrevious) {
        this.mPrevious = mPrevious;
    }

    public ArrayList<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(ArrayList<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }
}
