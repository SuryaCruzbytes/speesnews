package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;

public class SubCategory {

    @SerializedName("id")
    private int mId;
    @SerializedName("uid")
    private String mUid;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("description")
    private String mDescription;

    @SerializedName("image")
    private String mImage;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("video_url")
    private String mVideoUrl;
    @SerializedName("likes_count")
    private int mLikesCount;
    @SerializedName("dislikes_count")
    private int mDislikeCount;

    @SerializedName("created_at")
    private String mCreatedOn;
    @SerializedName("reacted")
    private int mReacted;

    @SerializedName("is_bookmarked")
    private boolean mBookmarks;

    public SubCategory(int id, String title, String description, String image, int likecount, int dislikecount, String createdon, boolean bookmarks) {
        mId=id;
        mTitle=title;
        mDescription=description;
        mImage=image;
        mLikesCount=likecount;
        mDislikeCount=dislikecount;
        mCreatedOn=createdon;
        mBookmarks=bookmarks;
    }


    public String getmVideoUrl() {
        return mVideoUrl;
    }

    public void setmVideoUrl(String mVideoUrl) {
        this.mVideoUrl = mVideoUrl;
    }

    public int getmReacted() {
        return mReacted;
    }

    public void setmReacted(int mReacted) {
        this.mReacted = mReacted;
    }

    public boolean ismBookmarks() {
        return mBookmarks;
    }

    public boolean getmBookmarks() {
        return mBookmarks;
    }

    public void setmBookmarks(boolean mBookmarks) {
        this.mBookmarks = mBookmarks;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmUid() {
        return mUid;
    }

    public void setmUid(String mUid) {
        this.mUid = mUid;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public String getmUrl() {
        return mUrl;
    }

    public void setmUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public int getmLikesCount() {
        return mLikesCount;
    }

    public void setmLikesCount(int mLikesCount) {
        this.mLikesCount = mLikesCount;
    }

    public int getmDislikeCount() {
        return mDislikeCount;
    }

    public void setmDislikeCount(int mDislikeCount) {
        this.mDislikeCount = mDislikeCount;
    }

    public String getmCreatedOn() {
        return mCreatedOn;
    }

    public void setmCreatedOn(String mCreatedOn) {
        this.mCreatedOn = mCreatedOn;
    }
}
