package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 20/09/17
 */

public class SocialLogin {

    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("client")
    private String mClient;

    public SocialLogin(String accessToken, String client) {
        mAccessToken = accessToken;
        mClient = client;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public void setAccessToken(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getClient() {
        return mClient;
    }

    public void setClient(String client) {
        mClient = client;
    }
}
