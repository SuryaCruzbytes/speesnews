package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;


public class Bookmarks {

    @SerializedName("news_blog_id")
    private int mBlogId;
    @SerializedName("news_blog")
    private NewsBlog newsBlog;

    @SerializedName("id")
    private int mId;
    public Bookmarks(int id) {
        mBlogId = id;

    }
    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int getmBlogId() {
        return mBlogId;
    }

    public void setmBlogId(int mBlogId) {
        this.mBlogId = mBlogId;
    }

    public NewsBlog getNewsBlog() {
        return newsBlog;
    }

    public void setNewsBlog(NewsBlog newsBlog) {
        this.newsBlog = newsBlog;
    }

    public class NewsBlog
    {
        @SerializedName("id")
        private int id;

        private String mUid;
        @SerializedName("title")
        private String mTitle;
        @SerializedName("description")
        private String mDescription;

        @SerializedName("image")
        private String mImage;
        @SerializedName("url")
        private String mUrl;
        @SerializedName("likes_count")
        private int mLikesCount;
        @SerializedName("dislikes_count")
        private int mDislikeCount;

        @SerializedName("created_at")
        private String mCreatedOn;
        @SerializedName("reacted")
        private int mReacted;
        @SerializedName("video_url")
        private String mVideoUrl;
        @SerializedName("is_bookmarked")
        private boolean isBookmarks;

        public String getmVideoUrl() {
            return mVideoUrl;
        }

        public void setmVideoUrl(String mVideoUrl) {
            this.mVideoUrl = mVideoUrl;
        }

        public boolean isBookmarks() {
            return isBookmarks;
        }

        public void setBookmarks(boolean bookmarks) {
            isBookmarks = bookmarks;
        }

        public int getmReacted() {
            return mReacted;
        }

        public void setmReacted(int mReacted) {
            this.mReacted = mReacted;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getmUid() {
            return mUid;
        }

        public void setmUid(String mUid) {
            this.mUid = mUid;
        }

        public String getmTitle() {
            return mTitle;
        }

        public void setmTitle(String mTitle) {
            this.mTitle = mTitle;
        }

        public String getmDescription() {
            return mDescription;
        }

        public void setmDescription(String mDescription) {
            this.mDescription = mDescription;
        }

        public String getmImage() {
            return mImage;
        }

        public void setmImage(String mImage) {
            this.mImage = mImage;
        }

        public String getmUrl() {
            return mUrl;
        }

        public void setmUrl(String mUrl) {
            this.mUrl = mUrl;
        }

        public int getmLikesCount() {
            return mLikesCount;
        }

        public void setmLikesCount(int mLikesCount) {
            this.mLikesCount = mLikesCount;
        }

        public int getmDislikeCount() {
            return mDislikeCount;
        }

        public void setmDislikeCount(int mDislikeCount) {
            this.mDislikeCount = mDislikeCount;
        }

        public String getmCreatedOn() {
            return mCreatedOn;
        }

        public void setmCreatedOn(String mCreatedOn) {
            this.mCreatedOn = mCreatedOn;
        }
    }
}
