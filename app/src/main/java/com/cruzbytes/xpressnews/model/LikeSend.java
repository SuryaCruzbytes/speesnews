package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/14/2017.
 */

public class LikeSend {

    @SerializedName("vote")
    private int mVote;
    @SerializedName("id")
    private int mId;
    @SerializedName("data")
    private String mData;
    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public LikeSend(int vote) {
        mVote = vote;

    }

    public int getmVote() {
        return mVote;
    }

    public void setmVote(int mVote) {
        this.mVote = mVote;
    }

    public String getmData() {
        return mData;
    }

    public void setmData(String mData) {
        this.mData = mData;
    }
}
