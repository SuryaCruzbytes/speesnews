package com.cruzbytes.xpressnews.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.callback.CategoryCallback;


public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView mOperatorType;
    public LinearLayout main_layout;
    public ImageView mOperatorLogo;
    private CategoryCallback mCallback;

    public CategoryHolder(View itemView, CategoryCallback callback) {
        super(itemView);

        mOperatorType = itemView.findViewById(R.id.category_name);
        mOperatorLogo = itemView.findViewById(R.id.news_logo);

        mCallback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == itemView) {
            mCallback.onItemClick(getLayoutPosition());
        }

    }
}
