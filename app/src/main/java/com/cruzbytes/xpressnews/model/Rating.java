package com.cruzbytes.xpressnews.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 12/14/2017.
 */

public class Rating {

    @SerializedName("rating")
    private int mRating;
    @SerializedName("text")
    private String mfeedback;

    public Rating(int rating, String feedback) {
        mRating = rating;
        mfeedback = feedback;

    }

    public int getmRating() {
        return mRating;
    }

    public void setmRating(int mRating) {
        this.mRating = mRating;
    }

    public String getMfeedback() {
        return mfeedback;
    }

    public void setMfeedback(String mfeedback) {
        this.mfeedback = mfeedback;
    }
}
