package com.cruzbytes.xpressnews;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.callback.FragmentCallback;
import com.cruzbytes.xpressnews.fragments.FragmentFlipBookmarks;
import com.cruzbytes.xpressnews.fragments.FragmentFlipCategory;
import com.cruzbytes.xpressnews.helper.RevealAnimation;
import com.cruzbytes.xpressnews.model.FcmToken;

import java.util.Objects;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;
import static com.cruzbytes.xpressnews.app.MyActivity.launch;
import static com.cruzbytes.xpressnews.app.MyActivity.launchClearStack;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, FragmentCallback {

    public Context mContext;
    AlertDialog.Builder build;
    AlertDialog dailog;
    protected AlertDialog.Builder builder;
    TextView mTextUsername, mTextUsermail;
    //public TextView mTextHomeTitle;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private ImageView mImageSearch;
    private ActionBarDrawerToggle mDrawerToggle;
    private MyPreference myPreference;
    private ProgressDialog mProgressDialog;
    private RelativeLayout mLayoutLinkClick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        setupToolbar();
        setupDrawer();
        if (!myPreference.isNewsSkip()) {
            updateFcm();
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
//            case R.id.orders:
//                View v= item.getActionView();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startRevealActivity(View v) {
        //calculates the center of the View v you are passing
        int revealX = (int) (v.getX() + v.getWidth() / 2);
        int revealY = (int) (v.getY() + v.getHeight() / 2);

        //create an intent, that launches the second activity and pass the x and y coordinates
        Intent intent = new Intent(this, SearchActivity.class);
        intent.putExtra(RevealAnimation.EXTRA_CIRCULAR_REVEAL_X, revealX);
        intent.putExtra(RevealAnimation.EXTRA_CIRCULAR_REVEAL_Y, revealY);

        //just start the activity as an shared transition, but set the options bundle to null
        ActivityCompat.startActivity(this, intent, null);

        //to prevent strange behaviours override the pending transitions
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        return onNavClick(item.getItemId());
    }


    private void initObjects() {
        mContext = this;
        myPreference = new MyPreference(mContext);
        build = new AlertDialog.Builder(mContext);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mProgressDialog = new ProgressDialog(mContext);
        mToolbar = findViewById(R.id.toolbar);
        mImageSearch = findViewById(R.id.img_search);
        //   mTextHomeTitle = findViewById(R.id.txt_HomeTitle);
        mNavigationView = findViewById(R.id.nav_view);

        View header = mNavigationView.getHeaderView(0);

        mLayoutLinkClick = mNavigationView.findViewById(R.id.bottom_layout);
        Menu menu = mNavigationView.getMenu();

        MenuItem nav_gallery = menu.findItem(R.id.nav_logout);


        if (!myPreference.isNewsSkip()) {
            nav_gallery.setTitle("Log Out");
            nav_gallery.setIcon(R.drawable.ic_logout);
            nav_gallery.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    Logout_dialog();
                    return false;
                }
            });
        } else {
            nav_gallery.setTitle("Log In");
            nav_gallery.setIcon(R.drawable.ic_login);
            nav_gallery.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    launch(mContext, LoginActivity.class);
                    return false;
                }
            });
        }

        mTextUsername= header.findViewById(R.id.txt_emailname);
        mTextUsermail= header.findViewById(R.id.email_id);

        mTextUsername.setText(myPreference.getName());
        mTextUsermail.setText(myPreference.getEmail());
        mNavigationView.setItemIconTintList(null);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close);

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_home, new FragmentFlipCategory())
                .commit();
        //noinspection deprecation
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                        INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
            }
        });
    }

    private void initCallbacks() {
        mNavigationView.setNavigationItemSelectedListener(this);
        mImageSearch.setOnClickListener(this);
        mLayoutLinkClick.setOnClickListener(this);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("Home");
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mToolbar.setNavigationIcon(R.drawable.ic_menubar);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Thank You");
            builder.setMessage("Are you sure you want to exit?");
            builder.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {

                            dialog.dismiss();
                        }
                    });
            builder.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//***Change Here***
                            startActivity(intent);
                            finish();

                        }
                    });

            builder.show();
        }
    }


    private boolean onNavClick(int itemId) {

        switch (itemId) {

            case R.id.nav_home:
                launchFragment(new FragmentFlipCategory(), true);
                return true;
            case R.id.nav_settings:
                launch(mContext, SettingsActivity.class);
                return true;
            case R.id.nav_Categories:
                launch(mContext, CategoriesActivity.class);
                return true;
            case R.id.nav_mybookmarks:
                launchFragment(new FragmentFlipBookmarks(), true);

                return true;
            case R.id.nav_invite:
                shareApp();
                return true;
            case R.id.nav_feedbackc:
                launch(mContext, FeedbackActivity.class);
                return true;
            case R.id.nav_about_us:
                launch(mContext, AboutusActivity.class);
                return true;
            default:
                return false;
        }
    }

    private void shareApp() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,
                getString(R.string.app_name) + " https://play.google.com/store/apps/details?id="
                        + getPackageName());
        startActivity(Intent.createChooser(shareIntent, "Invite"));
    }

    private void setupDrawer() {
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }


    @Override
    public void onClick(View v) {
        if (v == mImageSearch) {
            startRevealActivity(v);
        } else if (v == mLayoutLinkClick) {
            String url = "http://cruzbytes.com/";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

    }

    @Override
    public void launchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_home, fragment);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void updateFcm() {
        if (myPreference.isTokenUploaded() || myPreference.getFcmToken() == null)
            return;

        Call<Void> call = getApiService().updateFcm("Token " + myPreference.getToken(),
                new FcmToken(UUID.randomUUID().toString(), myPreference.getFcmToken()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    myPreference.setTokenUploaded(true);
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                processFailure(mContext, t);
            }
        });
    }

    public void Logout_dialog() {
        build.setMessage("Are you sure want to logout now?");
        build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!myPreference.isNewsSkip()) {
                    logout();
                } else {
                    ToastBuilder.build(mContext, "Unable to Logout, Please Login");
                }

            }
        });
        build.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // TODO Auto-generated method stub
                dailog.dismiss();
            }
        });
        dailog = build.create();
        dailog.show();

    }


    private void logout() {
        showProgressDialog("Loading....");

        Call<Void> call = getApiService().logoutUser("Token " + myPreference.getToken());

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    hideProgressDialog();
                    myPreference.clearUser();
                    launchClearStack(mContext, LoginActivity.class);
                    ToastBuilder.build(mContext, getString(R.string.alert_logged_out));
                    finish();
                    dailog.dismiss();
                }
                else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);
            }
        });
    }
    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
