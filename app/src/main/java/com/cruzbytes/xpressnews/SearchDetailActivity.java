package com.cruzbytes.xpressnews;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aphidmobile.flip.FlipViewController;
import com.cruzbytes.xpressnews.adapter.FlipperSearchAdapter;
import com.cruzbytes.xpressnews.api.ApiClient;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.callback.NewsFeedCallback;
import com.cruzbytes.xpressnews.model.Bookmarks;
import com.cruzbytes.xpressnews.model.LikeSend;
import com.cruzbytes.xpressnews.model.SubCategory;
import com.cruzbytes.xpressnews.model.SubcategoryList;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;

public class SearchDetailActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, NewsFeedCallback, Runnable {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    protected MyPreference mPreference;
    protected LinearLayout mLayoutEmpty;
    protected FlipViewController flipViewController;
    protected String mUrl;
    String litle, shortDesc;
    TextView mTextTitle;
    private NewsFeedCallback newsFeedCallback;
    private ArrayList<SubCategory> mSubcategory;
    private Context mContext;
    private ProgressDialog mProgressDialog;
    private FlipperSearchAdapter adapter;
    private String mSearchKey;
    private ImageView mImageClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_detail);
        processBundle();
        initObjects();
        initCallbacks();
        setUrl();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initCallbacks() {
        mImageClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == mImageClose) {
            onBackPressed();
        }

    }

    private void setUrl() {
        mUrl = ApiClient.BASE_URL + "news-blog/?q=" + mSearchKey;
        showProgressDialog("Loading...");

        if (!mPreference.isNewsSkip()) {
            getSubcategory(mUrl);
            Log.e("getSubcategory", "" + mUrl);
        } else {
            getSubcategorySkip(mUrl);
            Log.e("getSubcategorySkip", "" + mUrl);
        }
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            mSearchKey = bundle.getString("SearchKey");

    }

    private void initObjects() {

        mLayoutEmpty = findViewById(R.id.empty);
        mImageClose = findViewById(R.id.img_arrow);
        flipViewController = findViewById(R.id.flip_view);
        mTextTitle = findViewById(R.id.txt_title);
        mTextTitle.setText(mSearchKey);
        mContext = this;
        mProgressDialog = new ProgressDialog(mContext);
        mPreference = new MyPreference(Objects.requireNonNull(mContext));
        mSubcategory = new ArrayList<>();

        newsFeedCallback = this;
        flipViewController.setOnViewFlipListener(new FlipViewController.ViewFlipListener() {
            @Override
            public void onViewFlipped(View view, int position) {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void getSubcategory(String url) {

        Call<SubcategoryList> call = getApiService().getSubCategory(url, "Token " + mPreference.getToken());
        call.enqueue(new Callback<SubcategoryList>() {
            @Override
            public void onResponse(@NonNull Call<SubcategoryList> call, @NonNull Response<SubcategoryList> response) {
                SubcategoryList productResultResponse = response.body();
                if (response.isSuccessful() && productResultResponse != null) {
                    hideProgressDialog();
                    if (productResultResponse.getSubCategories().size() > 0) {
                        mSubcategory.clear();
                        mSubcategory.addAll(productResultResponse.getSubCategories());
                        adapter = new FlipperSearchAdapter(mContext, mSubcategory, newsFeedCallback);
                        flipViewController.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        ToastBuilder.build(mContext, "Sorry, Please try some different keywords");
                        onBackPressed();
                    }
                } else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubcategoryList> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);

            }
        });
    }

    @Override
    public void onRefresh() {
        setUrl();
    }

    @Override
    public void run() {
        setUrl();
    }

    @SuppressWarnings("SameParameterValue")
    private void showProgressDialog(String message) {
        mProgressDialog.setMessage(message);
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onBookmarkClick(int position) {
        SubCategory subCategory = mSubcategory.get(position);
        getBookmarks(new Bookmarks(subCategory.getmId()));

    }

    @Override
    public void onRating(int rating, int position) {
        getlike(new LikeSend(rating), position);
    }


    @Override
    public void onShareClick(int position, View v) {
        SubCategory subCategory = mSubcategory.get(position);

        litle = subCategory.getmTitle();
        shortDesc = subCategory.getmDescription();
        if (checkAndRequestPermissions()) {
            takeScreenshot();
        } else {
            requestStoragePermission();
        }
    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(Objects.requireNonNull(this), Manifest.permission.READ_EXTERNAL_STORAGE);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(Objects.requireNonNull(this),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_ID_MULTIPLE_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takeScreenshot();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void takeScreenshot() {
        Random r = new Random();
        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + r.nextInt() + ".jpeg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            String filePath = imageFile.getPath();

            Log.e("dkhdf", "" + filePath);
            File file = new File(filePath);
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_STREAM, uri);
            intent.putExtra(Intent.EXTRA_TEXT, litle);
            startActivity(Intent.createChooser(intent, "Share Blog"));

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTitleClick(int position) {
        SubCategory subCategory = mSubcategory.get(position);
        if (subCategory.getmUrl() != null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(subCategory.getmUrl()));
            startActivity(browserIntent);
        }

    }

    private void getBookmarks(Bookmarks bookmarks) {
        Call<Bookmarks> call = getApiService().bookmarks("Token " + "3740c2783b2de08f5b878437dbbfbe98561af7c3", bookmarks);
        call.enqueue(new Callback<Bookmarks>() {
            @Override
            public void onResponse(@NonNull Call<Bookmarks> call, @NonNull Response<Bookmarks> response) {
                Bookmarks userResponse = response.body();
                Log.e("/", "" + response.toString());
                if (response.isSuccessful() && userResponse != null) {
                    int feedId = Objects.requireNonNull(userResponse).getNewsBlog().getId();
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        SubCategory newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getmId() == feedId) {
                            newsfeeds1.setmBookmarks(true);

                            ToastBuilder.build(mContext, "Bookmarks added successfully");
                        }
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Bookmarks> call, @NonNull Throwable t) {
                processFailure(mContext, t);
            }
        });
    }

    private void getlike(LikeSend likeSend, int position) {

        Call<LikeSend> call = getApiService().getlike("Token " + mPreference.getToken(), position, likeSend);
        call.enqueue(new Callback<LikeSend>() {

            @Override
            public void onResponse(@NonNull Call<LikeSend> call, @NonNull Response<LikeSend> response) {
                LikeSend likePojo = response.body();
                Log.e("ddkhsdjSearch", "" + response.toString());
                if (response.isSuccessful()) {
                    int feedId = Objects.requireNonNull(likePojo).getmId();
                    for (int i = 0; i < mSubcategory.size(); i++) {
                        SubCategory newsfeeds1 = mSubcategory.get(i);
                        if (newsfeeds1.getmId() == feedId) {
                            newsfeeds1.setmReacted(likePojo.getmVote());
                            adapter.notifyDataSetChanged();
                        }


                    }
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));

                }
            }

            @Override
            public void onFailure(@NonNull Call<LikeSend> call, @NonNull Throwable t) {

                processFailure(mContext, t);

            }
        });
    }


    private void getSubcategorySkip(String url) {
        Call<SubcategoryList> call = getApiService().getSubCategorySkip(url);

        call.enqueue(new Callback<SubcategoryList>() {
            @Override
            public void onResponse(@NonNull Call<SubcategoryList> call, @NonNull Response<SubcategoryList> response) {
                SubcategoryList productResultResponse = response.body();
                if (response.isSuccessful() && productResultResponse != null) {
                    hideProgressDialog();
                    if (productResultResponse.getSubCategories().size() > 0) {
                        mSubcategory.clear();
                        mSubcategory.addAll(productResultResponse.getSubCategories());
                        adapter = new FlipperSearchAdapter(mContext, mSubcategory, newsFeedCallback);
                        flipViewController.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        hideProgressDialog();
                        ToastBuilder.build(mContext, "Sorry, Please try some different keywords");
                        onBackPressed();
                    }


                } else {
                    hideProgressDialog();
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<SubcategoryList> call, @NonNull Throwable t) {
                hideProgressDialog();
                processFailure(mContext, t);

            }
        });
    }
}
