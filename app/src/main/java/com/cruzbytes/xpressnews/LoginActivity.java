package com.cruzbytes.xpressnews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;
import com.cruzbytes.xpressnews.model.SocialLogin;
import com.cruzbytes.xpressnews.model.User;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.cruzbytes.xpressnews.api.ApiClient.getApiService;
import static com.cruzbytes.xpressnews.api.ErrorHandler.processError;
import static com.cruzbytes.xpressnews.api.FailureHandler.processFailure;
import static com.cruzbytes.xpressnews.app.MyActivity.launchClearStack;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        FacebookCallback<LoginResult>, GoogleApiClient.OnConnectionFailedListener {

    private static final int REQUEST_GOOGLE_LOGIN = 4;
    private Context mContext;
    private TextView mTextViewGoogle, mTextViewFacebook, mTextViewSkip;
    private CallbackManager mCallbackManager;
    private GoogleApiClient mGoogleApiClient;
    private FrameLayout mLayoutLoading;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initCallbacks();
        buildGoogleApiClient();

    }

    private void initCallbacks() {
        mTextViewGoogle.setOnClickListener(this);
        mTextViewFacebook.setOnClickListener(this);
        mTextViewSkip.setOnClickListener(this);

        LoginManager.getInstance().registerCallback(mCallbackManager, this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void initObjects() {
        mContext = this;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        mTextViewGoogle = findViewById(R.id.txt_google);
        mTextViewFacebook = findViewById(R.id.txt_fb);
        mTextViewSkip = findViewById(R.id.txt_skip);
        mLayoutLoading = findViewById(R.id.loading);
        mCallbackManager = CallbackManager.Factory.create();
        mPreference = new MyPreference(mContext);

        getHashkey();
    }

    @Override
    public void onClick(View v) {
        if (v == mTextViewFacebook) {
            startFbLogin();
        } else if (v == mTextViewGoogle) {
            startGoogleLogin();
        } else if (v == mTextViewSkip) {
            mPreference.setNewsSkip(true);
            launchClearStack(mContext, MainActivity.class);
        }
    }

    private void startFbLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("email", "public_profile"));
    }

    private void startGoogleLogin() {
        setLoading(true);
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, REQUEST_GOOGLE_LOGIN);
    }

    public void getHashkey() {
        try {
            @SuppressLint("PackageManagerGetSignatures")
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = null;
                try {
                    md = MessageDigest.getInstance("SHA");

                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                if (md != null) {
                    md.update(signature.toByteArray());
                }

                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("jdshfdlf", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        }
    }


    @Override
    public void onSuccess(LoginResult loginResult) {
        setLoading(true);
        fbLogin(new SocialLogin(loginResult.getAccessToken().getToken(),
                UUID.randomUUID().toString()));
    }

    @Override
    public void onCancel() {

        ToastBuilder.failure(mContext, getString(R.string.error_fb_cancelled));
    }

    @Override
    public void onError(FacebookException error) {
        ToastBuilder.failure(mContext, error.getMessage());

    }

    private void buildGoogleApiClient() {
        GoogleSignInOptions loginOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, loginOptions)
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GOOGLE_LOGIN) {
            setLoading(false);
            try {
                Task<GoogleSignInAccount> result = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount account = result.getResult(ApiException.class);
                if (account != null && account.getIdToken() != null) {

                    Log.e("accesstoken", "" + account.getIdToken());
                    googleLogin(new SocialLogin(account.getIdToken(), UUID.randomUUID().toString()));
                }
            } catch (ApiException er) {
                setLoading(false);
                Log.e("errorcode", "" + er.getStatusCode());
                er.printStackTrace();
            }
        } else {
            setLoading(false);
            Log.e("login", "" + data.toString());
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void googleLogin(SocialLogin socialLogin) {
        Call<User> call = getApiService().googleLogin(socialLogin);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    ToastBuilder.success(mContext, "Welcome, " + userResponse.getFirstName());
                    mPreference.setName(userResponse.getFirstName());
                    mPreference.setEmail(userResponse.getEmail());
                    mPreference.setToken(userResponse.getToken());
                    mPreference.setNewsSkip(false);
                    launchClearStack(mContext, MainActivity.class);
                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }
            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(mContext, t);
            }
        });
    }

    private void fbLogin(SocialLogin socialLogin) {
        Call<User> call = getApiService().fbLogin(socialLogin);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    ToastBuilder.success(mContext, "Welcome, " + userResponse.getFirstName());
                    mPreference.setName(userResponse.getFirstName());
                    mPreference.setEmail(userResponse.getEmail());
                    mPreference.setToken(userResponse.getToken());
                    mPreference.setNewsSkip(false);
                    launchClearStack(mContext, MainActivity.class);

                } else {
                    processError(mContext, response.code(), Objects.requireNonNull(response.errorBody()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(mContext, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        mLayoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
