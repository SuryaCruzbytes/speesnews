package com.cruzbytes.xpressnews.api;


import android.content.Context;

import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.app.MyPreference;
import com.cruzbytes.xpressnews.app.ToastBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;

/**
 * Created by Bobby on 21/09/17
 */

public class ErrorHandler {
    public static void processError(Context context, int statusCode, ResponseBody body) {
        MyPreference preference = new MyPreference(context);
        String message = null;
        try {
            JSONObject jsonObject = new JSONObject(body.string());
            if (jsonObject.has("non_field_errors")) {
                JSONArray jsonArray = jsonObject.getJSONArray("non_field_errors");
                message = jsonArray.getString(0);
            } else if (jsonObject.has("detail")) {
                message = jsonObject.getString("detail");
            } else {
                for (int i = 0; i < jsonObject.length(); i++) {
                    JSONArray keyArray = new JSONArray(
                            jsonObject.getString(jsonObject.names().getString(i)));
                    message = keyArray.getString(0);
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        switch (statusCode) {
            case 400:
                if (message != null) {
                    ToastBuilder.failure(context, message);
                }
                break;
            case 401:
                if (message != null) {
                    ToastBuilder.failure(context, message);
                }
                break;
            case 403:
                if (message != null) {
                    ToastBuilder.failure(context, message);
                }
                break;
            case 404:
                if (message != null) {
                    ToastBuilder.failure(context, message);
                }
                break;
            case 500:
                ToastBuilder.failure(context, context.getString(R.string.error_500));
                break;
            default:
                ToastBuilder.failure(context, context.getString(R.string.error_unexpected));
                break;
        }
    }
}
