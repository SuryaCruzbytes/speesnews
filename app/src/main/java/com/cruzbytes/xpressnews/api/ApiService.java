package com.cruzbytes.xpressnews.api;


import com.cruzbytes.xpressnews.model.Bookmarks;
import com.cruzbytes.xpressnews.model.CategoryPojo;
import com.cruzbytes.xpressnews.model.FcmToken;
import com.cruzbytes.xpressnews.model.LikeSend;
import com.cruzbytes.xpressnews.model.Profile;
import com.cruzbytes.xpressnews.model.Rating;
import com.cruzbytes.xpressnews.model.SocialLogin;
import com.cruzbytes.xpressnews.model.SubcategoryList;
import com.cruzbytes.xpressnews.model.User;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface ApiService {

    @POST("auth/google/")
    Call<User> googleLogin(@Body SocialLogin socialLogin);

    @POST("auth/facebook/")
    Call<User> fbLogin(@Body SocialLogin socialLogin);


    @GET("news-blog/category")
    Call<ArrayList<CategoryPojo>> getCategory();


    @POST("news-blog/feedback/")
    Call<Rating> RatingFeed(@Header("Authorization") String token, @Body Rating rating);

    @GET
    Call<SubcategoryList> getSubCategory(@Url String url, @Header("Authorization") String token);


    @GET
    Call<SubcategoryList> getSubCategorySkip(@Url String url);

    @POST("news-blog/bookmark/")
    Call<Bookmarks> bookmarks(@Header("Authorization") String token, @Body Bookmarks bookmarks);

    @DELETE
    Call<Bookmarks> deleteBookmarks(@Url String url, @Header("Authorization") String token);

    @GET("news-blog/bookmark/")
    Call<ArrayList<Bookmarks>> getMyBookmarks(@Header("Authorization") String token);

    @POST("news-blog/{id}/vote/")
    Call<LikeSend> getlike(@Header("Authorization") String token, @Path("id") int productId,
                           @Body LikeSend likeSend);


    @PATCH("auth/update-device-token/")
    Call<Void> updateFcm(@Header("Authorization") String token, @Body FcmToken fcmToken);

    @PATCH("auth/update-profile/")
    Call<Profile> UpdateProfile(@Header("Authorization") String token, @Body Profile profile);

    @POST("auth/logout/")
    Call<Void> logoutUser(@Header("Authorization") String token);
}
