package com.cruzbytes.xpressnews.api;

import android.content.Context;

import com.cruzbytes.xpressnews.R;
import com.cruzbytes.xpressnews.app.ToastBuilder;

import static com.cruzbytes.xpressnews.helper.ConnectivityStatus.isConnected;

/**
 * Created by admin on 10/20/2017
 */

public class FailureHandler {
    public static void processFailure(Context context, Throwable throwable) {
        if (isConnected(context)) {
            ToastBuilder.failure(context, context.getString(R.string.error_unexpected));
        } else {
            ToastBuilder.failure(context, context.getString(R.string.error_internet));
        }
    }
}
