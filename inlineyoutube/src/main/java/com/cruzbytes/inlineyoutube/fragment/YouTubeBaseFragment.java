
package com.cruzbytes.inlineyoutube.fragment;

import android.support.annotation.Nullable;

import com.cruzbytes.inlineyoutube.listener.YouTubeEventListener;


public interface YouTubeBaseFragment {

    void setYouTubeEventListener(@Nullable YouTubeEventListener listener);

    void release();
}
